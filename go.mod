module gitlab.com/mrdalv/go-lang-study

go 1.17

require (
	github.com/ilyakaznacheev/cleanenv v1.2.6
	github.com/julienschmidt/httprouter v1.3.0
	github.com/stretchr/testify v1.7.0
)

require (
	github.com/BurntSushi/toml v0.3.1 // indirect
	github.com/davecgh/go-spew v1.1.0 // indirect
	github.com/joho/godotenv v1.3.0 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/stretchr/objx v0.1.0 // indirect
	gopkg.in/yaml.v3 v3.0.0-20210107192922-496545a6307b // indirect
	olympos.io/encoding/edn v0.0.0-20201019073823-d3554ca0b0a3 // indirect
)
