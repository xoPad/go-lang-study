package main

import (
	//"strconv"
	"testing"

	"github.com/stretchr/testify/assert"
	//"github.com/stretchr/testify/suite"
	//"golang.org/x/tools/go/expect"
)

/*type EstimateValueTestSuite struct {
	suite.Suite
	Small  int
	Medium int
	Big    int
}

func (suite *EstimateValueTestSuite) EstimateValueSetupTest() {
	suite.Small = 5
	suite.Medium = 90
	suite.Big = 10101
}

func (suite *EstimateValueTestSuite) TestSmall() {
	suite.Equal("small", EstimateValue(suite.Small))
}

func (suite *EstimateValueTestSuite) TestMedium() {
	suite.Equal("medium", EstimateValue(suite.Medium))
}

func (suite *EstimateValueTestSuite) TestBig() {
	suite.Equal("big", EstimateValue(suite.Big))
	suite.T().Logf("TEST BIG VALUE OF BIG - " + strconv.Itoa(suite.Big))
}

func TestEstimateValueTestSuite(t *testing.T) {
	// чтобы go test смог запустить сьют, нужно создать обычную тестовую функцию
	// и вызвать в ней suite.Run
	some := new(EstimateValueTestSuite)
	some.EstimateValueSetupTest()
	suite.Run(t, some)
}*/

// table driven tests - testing method
func TestEstimateValueTableDriven(t *testing.T) {
	testCases := []struct {
		Name           string
		InputValue     int
		ExpectedAnswer string
	}{
		{
			Name:           "Big",
			InputValue:     10000,
			ExpectedAnswer: "big",
		},
		{
			Name:           "Small",
			InputValue:     1,
			ExpectedAnswer: "small",
		},
		{
			Name:           "Medium",
			InputValue:     88,
			ExpectedAnswer: "medium",
		},
	}

	for _, tt := range testCases {
		t.Run(tt.Name, func(t *testing.T) {
			assert.EqualValues(t, tt.ExpectedAnswer, EstimateValue(tt.InputValue))
		})
	}
}

func EstimateValue(value int) string {
	switch {
	case value < 10:
		return "small"

	case value < 100:
		return "medium"

	default:
		return "big"
	}
}
