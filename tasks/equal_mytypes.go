package tasks

import (
	"fmt"
	"reflect"
)

type MyType struct {
	IntField   int
	StrField   string
	PtrField   *float64
	SliceField []int
}

func (mt MyType) IsEqual(mt2 MyType) bool {
	return reflect.DeepEqual(mt, mt2)
}

func IsNil(obj interface{}) bool {
	if obj == nil {
		return true
	}

	// Value - мета-информацию о любом Go-значении (значение переменной, поля, функция)
	/*
		varInt := 100
		varIntValue := reflect.ValueOf(varInt)
		fmt.Println(varIntValue.IsZero()) // false
		fmt.Println(varIntValue.Int())    // 100


		var varBool *bool
		fmt.Println(reflect.ValueOf(varBool).IsNil())  // true

		trueVal := true
		reflect.ValueOf(&varBool).Elem().Set(reflect.ValueOf(&trueVal))

		fmt.Println(reflect.ValueOf(varBool).IsNil())       // false
		fmt.Println(reflect.ValueOf(varBool).Elem().Bool()) // true — получить значение через рефлексию
		fmt.Println(*varBool)                               // true — получить значение без рефлексии
	*/

	// Тип reflect.Type содержит описание Go-типа. Тип reflect.Kind задаёт множество базовых типов Go (структура, канал, слайс, функция, массив и т. д.).
	/*
		var varBool *bool
		fmt.Println(reflect.ValueOf(varBool).Kind()) // ptr
		fmt.Println(reflect.ValueOf(varBool).Type()) // *bool

		var varFloat float32
		fmt.Println(reflect.ValueOf(varFloat).Kind()) // float32
		fmt.Println(reflect.ValueOf(varFloat).Type()) // float32

		var varMap map[string]int
		fmt.Println(reflect.ValueOf(varMap).Kind()) // map
		fmt.Println(reflect.ValueOf(varMap).Type()) // map[string]int

		varStruct := struct{Value int}{}
		fmt.Println(reflect.ValueOf(varStruct).Kind()) // struct
		fmt.Println(reflect.ValueOf(varStruct).Type()) // struct { Value int }
	*/
	objValue := reflect.ValueOf(obj)
	if objValue.Kind() == reflect.Ptr && objValue.IsNil() {
		return true
	}

	return false
}

func EqualMyType() {
	floatValue1, floatValue2 := 10.0, 10.0
	a := MyType{IntField: 1, StrField: "str", PtrField: &floatValue1, SliceField: []int{1}}
	b := MyType{IntField: 1, StrField: "str", PtrField: &floatValue2, SliceField: []int{1}}

	fmt.Printf("Равенство a и b: %v\n", a.IsEqual(b))
	fmt.Printf("Проверка типа (%v) на nil: %v\n", reflect.TypeOf(a), IsNil(a))
}
