package tasks

import (
	"bytes"
	"crypto/md5"
	"fmt"
	"io"
	"strings"
)

type Comparer struct {
	BodyLegacy io.Reader
	BodyNew    io.Reader
}

func (c *Comparer) Read(p []byte) (int, error) {
	var (
		buf    = &bytes.Buffer{}
		hasher = md5.New()
	)

	// считываем ответ старого сервиса в буфер, затем считаем md5-чексумму
	_, err := io.Copy(buf, c.BodyLegacy)
	if err != nil {
		return 0, fmt.Errorf("failed read legacy body: %w", err)
	}

	//io.Copy(hasher, buf)

	checksumBodyLegacy := hasher.Sum(buf.Bytes())
	fmt.Printf("Buf %b", checksumBodyLegacy)
	buf.Reset()

	// считываем ответ нового сервиса и считаем его md5-чексумму
	hasher.Reset()
	_, err = io.Copy(buf, c.BodyNew)
	if err != nil {
		fmt.Printf("warning: failed read new body: %v\n", err)
		return buf.Read(p)
	}

	checksumBodyNew := hasher.Sum(buf.Bytes())
	fmt.Printf("Buf %b", checksumBodyNew)

	// сравниваем чексуммы
	if !bytes.Equal(checksumBodyLegacy, checksumBodyNew) {
		fmt.Printf("warning: checksums differ\n")
	}

	// возвращаем ответ старого сервиса
	return buf.Read(p)
}

func CheckSum() {
	var str string = "hello"

	firstData := strings.NewReader(str)
	secondData := strings.NewReader("hello")

	data := []byte(str)

	c := Comparer{firstData, secondData}

	fmt.Print(c.Read(data))
}
