package tasks

import (
	"fmt"
	"time"
)

type stopwatch struct {
	t     []time.Duration
	timer *time.Timer
	end   time.Time
}

func (sw *stopwatch) Start() {
	sw.timer = time.NewTimer(1 * time.Millisecond)
	sw.end = time.Now().Add(1 * time.Microsecond)
}

func (sw *stopwatch) SaveSplit() {
	sw.t = append(sw.t, -sw.end.Sub(time.Now()))
}

func (sw *stopwatch) GetResults() []time.Duration { return sw.t }

func RealFunc() {
	sw := stopwatch{}
	sw.Start()

	time.Sleep(1 * time.Second)
	sw.SaveSplit()

	time.Sleep(500 * time.Millisecond)
	sw.SaveSplit()

	time.Sleep(300 * time.Millisecond)
	sw.SaveSplit()

	fmt.Println(sw.GetResults())
}
