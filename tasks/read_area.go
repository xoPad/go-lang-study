package tasks

import "fmt"

type figures int

const (
	square   figures = iota // квадрат
	circle                  // круг
	triangle                // равносторонний треугольник
)

func area(figure figures) (func(float64) float64, bool) {
	switch figure {
	case square:
		return func(f float64) float64 { return f * f }, true

	case circle:
		return func(f float64) float64 { return 3.142 * f * f }, true

	case triangle:
		return func(f float64) float64 { return 0.433 * f * f }, true

	default:
		return nil, false

	}
}

func ReadArea(figure figures, value float64) {
	ar, ok := area(figure)
	if !ok {
		fmt.Println("Ошибка")
	}
	myArea := ar(value)

	fmt.Println(myArea)
}
