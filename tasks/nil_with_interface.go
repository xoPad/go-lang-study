package tasks

import (
	"fmt"
	"time"
)

type MyError struct { // ...
}

func (e *MyError) Error() string {
	return "..."
}

var ErrFriday13 = &MyError{}

func checkTodayIsOkay() error {
	// Код выведет строчку "error is not nil", потому что Go обернёт nil-указатель *MyError в не-nil-интерфейс error
	// var err *MyError
	// Исправить можно так - В этом случае код выведет строчку "error is nil", потому что сам интерфейс error будет nil
	var err error

	t := time.Now()
	if t.Weekday() == time.Friday && t.Day() == 13 {
		err = ErrFriday13
	}

	return err
}

func RealizeCheckToday() {
	err := checkTodayIsOkay()
	if err != nil {
		fmt.Println("error is not nil")
		return
	}

	fmt.Println("error is nil")
}
