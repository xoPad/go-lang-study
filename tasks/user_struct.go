package tasks

import (
	"fmt"
	"time"
)

type User struct {
	Email      string
	Name       string
	LastAccess time.Time
}

func (u User) String() string {
	return "user with email " + u.Email
}

func Printf(v fmt.Stringer) {
	fmt.Print("Это тип, реализующий Stringer, " + v.String())
}

func PrintUserEmail(u User) {
	Printf(u)
}
