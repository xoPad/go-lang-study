package tasks

import (
    "errors"
    "fmt"
    "reflect"
    "strconv"
    "strings"
)

var ErrReflect = errors.New("reflection error")

type (
    // valOpType — enum, задающий возможные значения для операторов валидации.
    valOpType string

    // valOp хранит данные, необходимые для оператора валидации.
    valOp struct {
        OpType  valOpType
        OpValue int
    }

    valOps []valOp
)

const (
    // имя тега
    tagValidate = "validate"

    // возможные значения тега
    // int
    valueGTE valOpType = "gte"
    valueGT  valOpType = "gt"
    valueLTE valOpType = "lte"
    valueLT  valOpType = "lt"
    // string
    valueMin valOpType = "min"
    valueMax valOpType = "max"
)

// IsValid валидирует enum-тип valOpType.
func (t valOpType) IsValid() bool {
    switch t {
    case valueGTE, valueGT, valueLTE, valueLT:
        return true
    case valueMin, valueMax:
        return true
    default:
        return false
    }
}

// ValidateInt валидирует значение поля структуры типа int.
func (ops valOps) ValidateInt(v int) error {
    for _, op := range ops {
        failed := false
        switch op.OpType {
        case valueGTE:
            if v < op.OpValue {
                failed = true
            }
        case valueGT:
            if v <= op.OpValue {
                failed = true
            }
        case valueLTE:
            if v > op.OpValue {
                failed = true
            }
        case valueLT:
            if v >= op.OpValue {
                failed = true
            }
        default:
            return fmt.Errorf("unsupported validation type (%s): %w", string(op.OpType), ErrReflect)
        }

        if failed {
            return fmt.Errorf("%s (%d) condition failed", string(op.OpType), op.OpValue)
        }
    }

    return nil
}

// ValidateString валидирует значение поля структуры типа string.
func (ops valOps) ValidateString(v string) error {
    for _, op := range ops {
        failed := false
        switch op.OpType {
        case valueMin:
            if len(v) < op.OpValue {
                failed = true
            }
        case valueMax:
            if len(v) > op.OpValue {
                failed = true
            }
        default:
            return fmt.Errorf("unsupported validation type (%s): %w", string(op.OpType), ErrReflect)
        }

        if failed {
            return fmt.Errorf("%s (%d) condition failed", string(op.OpType), op.OpValue)
        }
    }

    return nil
}

// ValidateStruct валидирует переданный объект типа структура (или указатель на структуру).
// Функция возвращает обёрнутую ошибку ErrReflect в случае неверного описания структуры или некорректного аргумента.
func ValidateStruct(obj interface{}) error {
    rv := reflect.ValueOf(obj)
    if rv.Kind() == reflect.Ptr {
        if rv.IsNil() {
            return fmt.Errorf("obj pointer is nil: %w", ErrReflect)
        }
        rv = rv.Elem()
    }

    rt := rv.Type()
    if rt.Kind() != reflect.Struct {
        return fmt.Errorf("obj is not a struct: %w", ErrReflect)
    }

    for fieldIdx := 0; fieldIdx < rt.NumField(); fieldIdx++ {
        field := rt.Field(fieldIdx)
        tag, found := field.Tag.Lookup(tagValidate)
        if !found {
            continue
        }

        valOps, err := parseValOps(tag)
        if err != nil {
            return fmt.Errorf("field (%s): parsing validation tag: %v: %w", field.Name, err, ErrReflect)
        }

        switch field.Type.Kind() {
        case reflect.Int:
            if err := valOps.ValidateInt(int(rv.Field(fieldIdx).Int())); err != nil {
                return fmt.Errorf("field (%s): int validation: %w", field.Name, err)
            }
        case reflect.String:
            if err := valOps.ValidateString(rv.Field(fieldIdx).String()); err != nil {
                return fmt.Errorf("field (%s): string validation: %w", field.Name, err)
            }
        default:
            return fmt.Errorf("field (%s): unsupported field type (%s): %w", field.Name, field.Type, ErrReflect)
        }
    }

    return nil
}

// parseValOps парсит и валидирует значение тега validate.
func parseValOps(tagValueRaw string) (retOps valOps, retErr error) {
    for _, value := range strings.Split(tagValueRaw, ",") {
        valueParts := strings.Split(value, " ")
        if len(valueParts) != 2 {
            retErr = fmt.Errorf("tag value (%s): wrong format ('op_type op_value')", value)
            return
        }

        opType := valOpType(valueParts[0])
        if !opType.IsValid() {
            retErr = fmt.Errorf("tag value (%s): unknown validation type (%s)", value, valueParts[0])
            return
        }

        opValue, err := strconv.Atoi(valueParts[1])
        if err != nil {
            retErr = fmt.Errorf("tag value (%s): parsing validation value (%s): %w", value, valueParts[1], err)
            return
        }

        retOps = append(retOps, valOp{
            OpType:  opType,
            OpValue: opValue,
        })
    }

    return
}

type TestStructMy struct {
    IntVal   int    `validate:"gte 100,lt 150"`
    StrVal   string `validate:"min 3"`
    IntOther int
}

func ValidateType() {
    check := func(comment string, s TestStructMy) {
        if err := ValidateStruct(s); err != nil {
            if errors.Is(err, ErrReflect) {
                fmt.Printf("%s: ошибка формата тегов валидации: %v\n", comment, err)
                return
            }
            fmt.Printf("%s: ошибка валидации: %v\n", comment, err)
            return
        }
        fmt.Printf("%s: OK\n", comment)
    }

    check("s1", TestStructMy{IntVal: 10, StrVal: "ABCDEFG"})
    check("s2", TestStructMy{IntVal: 110, StrVal: "AB"})
    check("s3", TestStructMy{IntVal: 110, StrVal: "ABC"})
}