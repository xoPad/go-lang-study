package tasks

import (
	"fmt"
	"math/rand"
	"sort"
	"time"
)

// CircularBuffer реализует структуру данных "кольцевой буфер" для float64 значений.
type CircularBuffer struct {
	values  []float64 // текущие значения буфера
	headIdx int       // индекс "головы" (первый непустой элемент)
	tailIdx int       // индекс "хвоста" (первый пустой элемент)
}

// GetCurrentSize возвращает текущую длину буфера.
func (b CircularBuffer) GetCurrentSize() int {
	if b.tailIdx < b.headIdx {
		return b.tailIdx + cap(b.values) - b.headIdx
	}

	return b.tailIdx - b.headIdx
}

// GetValues возвращает слайс текущих значений буфера сохраняя порядок записи.
func (b CircularBuffer) GetValues() (retValues []float64) {
	for i := b.headIdx; i != b.tailIdx; {
		retValues = append(retValues, b.values[i])
		if i++; i == cap(b.values) {
			i = 0
		}
	}

	return
}

// AddValue добавляет новое значение в буфер.
func (b *CircularBuffer) AddValue(v float64) {
	b.values[b.tailIdx] = v
	if b.tailIdx++; b.tailIdx == cap(b.values) {
		b.tailIdx = 0
	}
	if b.tailIdx == b.headIdx {
		if b.headIdx++; b.headIdx == cap(b.values) {
			b.headIdx = 0
		}
	}
}

// ForceSetValueByIdx выставляет значение буфера по индексу.
// TODO: убрать при первой же возможности.
func (b CircularBuffer) ForceSetValueByIdx(idx int, v float64) {
	b.values[idx] = v
}

// NewCircularBuffer - конструктор типа CircularBuffer.
func NewCircularBuffer(size int) CircularBuffer {
	return CircularBuffer{values: make([]float64, size+1)}
}

// MovingMedian - "наследник" типа CircularBuffer реализующий скользящую медиану.
type MovingMedian struct {
	CircularBuffer
}

// GetMedian возвращает текущее значение медианного фильтра.
func (m MovingMedian) GetMedian() float64 {
	values := m.GetValues()
	if len(values) == 0 {
		return 0.0
	}

	sort.Slice(values, func(i, j int) bool {
		return values[i] < values[j]
	})

	return values[len(values)/2]
}

// NewMovingMedian - конструктор типа MovingMedian.
func NewMovingMedian(size int) MovingMedian {
	return MovingMedian{
		CircularBuffer: NewCircularBuffer(size),
	}
}

func CircularBufferMain() {
	rand.Seed(time.Now().Unix())

	fmt.Println("CircularBuffer")
	buf := NewCircularBuffer(4)
	for i := 0; i < 6; i++ {
		if i > 0 {
			buf.AddValue(float64(i))
		}
		fmt.Printf("[%d]: %v\n", buf.GetCurrentSize(), buf.GetValues())
	}

	fmt.Println("MovingMedian")
	filter := NewMovingMedian(5)
	for i := 0; i < 7; i++ {
		if i > 0 {
			filter.AddValue(float64(rand.Intn(10)))
		}
		fmt.Printf("[%d]: %v: %v\n", filter.GetCurrentSize(), filter.GetValues(), filter.GetMedian())
	}
}
