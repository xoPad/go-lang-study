package tasks

import "fmt"

type MyTypePtr struct {
	IntField int
	StrField string
	PtrField *float64
}

func (mt MyTypePtr) IsEqual(mt2 MyTypePtr) bool {
	return mt == mt2
}

func EqualPtrMyType() {
	floatValue1, floatValue2 := 10.0, 10.0
	a := MyTypePtr{IntField: 1, StrField: "str", PtrField: &floatValue1}
	b := MyTypePtr{IntField: 1, StrField: "str", PtrField: &floatValue2}

	fmt.Printf("Равенство a и b: %v\n", a.IsEqual(b))
}